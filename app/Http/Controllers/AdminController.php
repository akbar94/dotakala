<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        if (Session::get('admin_email')) {

            return view('admin/dashboard');
        }
        else{
        $admin_email = $request->admin_email;
        $admin_password = md5($request->admin_password);
        
        $result = DB::table('tbl_admin')
        ->where('email', $admin_email)
        ->where('password',$admin_password)
        ->first();

        if ($result) {
            echo 'inside if';
            Session::put('admin_nickname',$result->nickname);
            Session::put('admin_id',$result->id);
            Session::put('admin_email',$result->email);

            return redirect('admin/dashboard');
        }
        else {
            echo 'inside else';
            Session::put('msg','email or username invalid!');
            return redirect('admin');
        }
    }
    }
    public function login()
    {
        if (Session::get('admin_email')) {
            return redirect('admin/dashboard');
        }
        else {
        
        
        return view('admin.login');
            }
    }
    public function logout(Request $request)
    {
        // $request->session()->forget('admin_email');
        // $request->session()->forget('msg');
        Session::flush();
        return redirect('admin');
    }
}
