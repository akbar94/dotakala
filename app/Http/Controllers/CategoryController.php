<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\Input;

session_start();
class CategoryController extends Controller
{
    public function add_category()
    {
        return view('admin.add_category');
    }
    public function save_category(Request $request)
    {
        $category = new Category;
        if ($file = $request->file('picture')) {
            $path = Storage::putFile('categories', $request->file('picture'),'public');
            $url = Storage::url($path);
            $category->name = $request->title;
            $category->picture = $path;
            if ($request->status == 'true') {
                $category->active = 1;
            }
            else{
                $category->active = 0;
            }
            $category->save();
            Session::put('msg', 'new category added successfully.');
            return Redirect::to('admin/add_category');
        }
    }
    public function all_category()
    {
        $categories = Category::all();
        return view('admin.all_category',compact('categories'));
    }
    public function category_active($category_id)
    {
        Category::where('id',$category_id)->update(['active'=>1]);
        Session::put('msg','category activated successfully');
        return Redirect::to('admin/all_category');   
    }
    public function category_unactive($category_id)
    {
        Category::where('id',$category_id)->update(['active'=>0]);
        Session::put('msg','category deactivated successfully');
        return Redirect::to('admin/all_category');
    }
    public function edit($category_id)
    {
        $category =  Category::where('id',$category_id)->first();
        return view('admin.edit_category', compact('category'));
    }
    public function save_update_category(Request $request , $category_id)
    {
        $category = Category::where('id',$category_id)->first();
        if ($request->title) {
                $category->name = $request->title;
        }
        if ($file = $request->file('picture')) {
            $path = Storage::putFile('categories', $request->file('picture'),'public');
            $url = Storage::url($path);
            $category->name = $request->title;
            $category->picture = $path;
    }
    if ($request->status == 'true') {
        $category->active = 1;
    }
    else{
        $category->active = 0;
    }
    $category->save();
    Session::put('msg', 'category updated successfully.');
    return Redirect::to('admin/all_category');

    }
    public function delete($category_id)
    {
        $category = Category::find($category_id);
        $isDeleted =  $category->delete();

        if ($isDeleted) {
            Session::put('msg','Category deleted successfully!');
            return Redirect::to('admin/all_category');
        }
        else {
            Session::put('msg','Category Could not be Deleted!');
            return Redirect::to('admin/category_all');
        }
    }
}
