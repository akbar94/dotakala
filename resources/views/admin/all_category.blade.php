@extends('admin.layout') 
@section('content')
<div class="col-lg-7>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Table Stripped</h4>
                <div class="table-responsive"> 
                    <table class="table table-bordered table-striped verticle-middle">
                            @php
                            if (Session::get('msg')) {
                                echo'<div class="alert alert-success">
                                    <p>'.Session::get('msg').'</p>
                                 </div>';
                                 Session::put('msg',null);
                            }

                    @endphp
                        <thead>
                            <tr>
                                <th scope="col">Category id</th>
                                <th scope="col">title</th>
                                <th scope="col">status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $category)
                                
                            
                            <tr>
                                <td>{{$category->id}}</td>
                                <td>{{$category->name}}</td>
                                @if ($category->active)
                                    <td><span class="badge badge-success px-2">Active</span></td>
                                @else 
                                    <td><span class="badge badge-danger px-2">Deactive</span></td>

                                @endif
                                <td>
                                    <span>
                                        @if ($category->active)
                                          <a href="{{URL::to('admin/'.$category->id.'/category_unactive')}}" data-toggle="tooltip" data-placement="top" title="Deactive"><i class="fa fa-close color-danger"></i></a>
                                            @else 
                                            <a href="{{URL::to('admin/'.$category->id.'/category_active')}}" data-toggle="tooltip" data-placement="top" title="active"><i class="fa fa-check-square"></i></a>
                                        @endif
                                        <a href="{{URL::to('admin/'.$category->id.'/category_edit')}}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil color-muted m-r-5"></i></a>
                                        <a href="{{URL::to('admin/'.$category->id.'/category_delete')}}" data-toggle="tooltip" data-placement="top" title="Delete" ><i class="fa fa-trash color-muted m-r-5"></i></a>

                                    </span>
                                </td>
                            </tr>

                            @endforeach

                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection