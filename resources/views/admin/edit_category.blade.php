@extends('admin.layout')
@section('content')
<div class="container-fluid">
<div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    @php
                            if (Session::get('msg')) {
                                echo'<div class="alert alert-success">
                                    <p>'.Session::get('msg').'</p>
                                 </div>';
                                 Session::put('msg',null);
                            }

                    @endphp
                    <h4 class="card-title">Edit {{$category->name}} Category</h4>
                    <div class="basic-form">
                    <form  action="{{URL::to('admin/'.$category->id.'/save_update_category')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                            <div class="form-group">
                            <input type="text" class="form-control input-default" name="title" placeholder="category name..." value="{{$category->name}}">
                            </div>
                            <div class="form-group">
                                    <h4 class="card-title">{{$category->name}} picture: </h4>
                                    <img src="{{asset('storage/'.$category->picture)}}" alt="Smiley face" height="60" width="60">
                                    <input type="file" name="picture" class="form-control-file">
                            </div>
                            <div class="form-check mb-3">
                                        <label class="form-check-label">
                                        <input type="checkbox" name="status" class="form-check-input" @if($category->active) checked @endif  value="true">status on</label>

                            </div>

                            <button class="btn btn-primary btn-user btn-success">update category</button>
                            {{-- <div class="form-group"><button type="button" class="btn mb-1 btn-primary btn-lg">enter</button></div> --}}
                                
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

        
@endsection