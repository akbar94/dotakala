<?php

//ForntEnd

Route::get('/','HomeController@index');



//blog
Route::get('/blog','BlogController@index');





//BackEnd
//admin panel routes ..............................................................//
Route::get('/admin','AdminController@login');
Route::post('/admin/dashboard','AdminController@index'); //when post admin id email
Route::get('/admin/dashboard','AdminController@index'); //when nothing posted
Route::get('/admin/logout','AdminController@logout'); //when admin loged out
Route::get('/admin/category','CategoryController@index');
Route::get('/admin/add_category','CategoryController@add_category');
Route::post('/admin/save_category','CategoryController@save_category');
Route::get('/admin/all_category','CategoryController@all_category');
Route::get('/admin/{category_id}/category_active','CategoryController@category_active');
Route::get('/admin/{category_id}/category_unactive','CategoryController@category_unactive');
Route::get('/admin/{category_id}/category_edit','CategoryController@edit');
Route::post('/admin/{category_id}/save_update_category','CategoryController@save_update_category');
Route::get('/admin/{category_id}/category_delete','CategoryController@delete');

